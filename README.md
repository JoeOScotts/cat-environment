# CAT Environment
The Cloud Application Team's project environment configuration package.

This project was designed to manage environment variables in various types of applications. The idea is to have a little configuration files as possible, fitting as many variables into a front-end-config.json and a back-end-config.json. The package currently supports configuration for Ionic Apps and Python Back end programs hosted on App Engine. 

We recommend this project be used with `Gulp` for automated processes, like switching between development and production environments.

Recommended Project Folder Structure:
```
front-end
|   (Front end application files)
back-end
|   (Back end application files)
environment
|   +-- options
|   |   +-- development
|   |       app.yaml
|   |       front-end-config.json
|   |       back-end-config.json
|   |       
|   |   +-- production
|   |       app.yaml
|   |       front-end-config.json
|   |       back-end-config.json
|   |
|   +-- ionic-images
|       production-icon.png
|       production-splash.png
|       development-icon.png
|       development-splash.png
```


## Getting Started
### Ionic Apps
1. Create your folder structure per the above recommendation.
    - Your front-end-config.json files for each environment option MUST have the following properties:
```js
    {
        "name": "development/production/pilot",
        "ionicApp": {
            "appId": "com.scotts.enterprise.something.prod/dev",
            "appName": "AppName/Appname - DEV",
            "version": "0.0.1"
        }
    }
```
2. Initialize a Node App in your root directory: `npm init`
3. Install gulp and create the `gulpfile.js`: `npm i -g gulp; npm i gulp --save-dev`
4. Install this repo: `npm i https://JoeOScotts@bitbucket.org/JoeOScotts/cat-environment.git`
5. In your Gulp file, require this package and initialize your environment by providing the directory locations:
```js
    const catEnvironment = require('cat-environment');
    
    // The initialize function takes an object which specifies the directory locations for your apps.
    catEnvironment.initialize({
        environment: './environment',
        ionicApp: './ionic-app',
        pythonBackEnd: './back-end'
        // (Future) angularApp: './angular-app'
    });

    const ionicAppController = catEnvironment.ionicApp();

    const backEndController = catEnvironment.pythonBackEnd();

    const developerRobot = catEnvironment.developerRobot();
```


# Examples

# Developer Robot
#### Instantiate the robot
```js
    const catEnvironment = require('cat-environment');
    const developerRobot = catEnvironment.developerRobot();
```
#### Copy a file to another directory:
```js
    developerRobot.copyFile(
        'path/to/source/file/random-name.json', 'path/to/destination/randomer-name.json'
    ).then(() => {
        // File's been moved!
    });
```
#### Run Linux commands
```js
    //Commands are run asynchronously using Promises
    const showConsoleOutput = true;
    developerRobot.runCommand('cd ionic-app && npm i lodash;', showConsoleOutput).then(() => {
        // The command ran successfully!
    }, error => {
        // The command exited due to an error
    });
```
##### Get the contents of a JSON file:
```js
    // This is better than require() statements or getFileSync functions
    // because we don't have to worry about dynamic folder paths or blocking I/O
    developerRobot.getJsonFile('./ionic-app/package.json').then(fileContents => {
        // Do something with contents of JSON file.
    });
```

# Ionic Apps 
#### Instantiate the Ionic app controller
```js
    const catEnvironment = require('cat-environment');
    catEnvironment.initialize({
        environment: './environment',
        ionicApp: './ionic-app'
        // + any other apps (pythonBackEnd, etc)
    });

    const myIonicApp = catEnvironment.ionicApp();
```
#### Change the environment configuration 
```js
    myIonicApp.changeEnvironment('development').then(() => {
        /* Environment changed! This does the following:
            1. Changes the config.xml's app id, app name and version.
            2. Moves the appropriate front-end-config.json file to the `ionic-app/src/environment` folder.
            3. Swaps the icon.png and splash.png images for the new environment ones.
            4. Reinstalls the Android and iOS platforms, which generates new icon and splash images from the new ones provided.
        */
    });
```
#### Install the node packages
```js
    myIonicApp.installPackages().then(() => {
        // Done!
        // The developer robot cd's into the ionic app directory and runs npm install
    });
```
#### Run a device emulator
```js
    // Command: gulp emulate --ios --device iPhone-7
    myIonicApp.emulate(args).then(() => {
        // Device emulator started!
        // Args are derived from the 'yargs' npm package's 'argv' property. This is integrated into gulp.
    });
```
#### Run the app on a connected device
```js
    // Command: gulp run --ios
    myIonicApp.run(args).then(() => {
        // Device running on connected iPhone! 
        // Args are derived from the 'yargs' npm package's 'argv' property. This is integrated into gulp.
    })
```

# Python Back End
#### Instantiate the python back end app
```js
    const catEnvironment = require('cat-environment');
    catEnvironment.initialize({
        environment: './environment',
        pythonBackEnd: './back-end'
        // + any other apps (ionicApp, etc)
    });

    const myPythonApp = catEnvironment.pythonBackEnd();
```
#### Change the environment configuration 
```js
    myPythonApp.changeEnvironment('development').then(() => {
        // Now in the development environment!
        /* Environment changed! This does the following:
            1. Copies the correct app.yaml into the python app directory
            2. Copies the correct back-end-config.json into the python app directory
            3. Copies the correct front-end-config.json into the python app directory
        */
    })
    
```