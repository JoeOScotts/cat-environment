'use strict';
const Shell = require('shelljs');
const Promise = require('promise');
const FsExtra = require('fs-extra');
const fs = require('fs');
const Gulp = require('gulp');
const GulpUtil = require('gulp-util');
const Cheerio = require('gulp-cheerio');
const path = require('path');

module.exports = class DeveloperRobot {
    constructor(
    ) {
    }
    

    /**
     * Log an action to the console while completing tasks. 
     * @param  {string} logText
     * @return void
     */
    logAction(logText) {
        return console.log('\r\n\r\n' + GulpUtil.colors.yellow(logText) + '\r\n');
    }


    /**
     * Log an error message to the console while completing tasks. 
     * @param  {string} logText
     * @return void
     */
    logError(logText) {
        return console.log('\r\n\r\n' +GulpUtil.colors.bold.red(logText) + '\r\n\r\n');
    }


    /**
     * Runs a Linux command and returns a promise when it completes. If the command fails, the promise is rejected.
     * @param  {string} commandString
     * @param  {boolean} doesLogOutput
     */
    runCommand(commandString, doesLogOutput) {
        this.logAction(`> ~ DeveloperRobot$: ${commandString}`);
        const isCommandSilent = !doesLogOutput;
        return new Promise((resolve, reject) => {
            Shell.exec(commandString, {
                silent: isCommandSilent
            }, 
            (exitCode, stdOut, stdError) => {
                if (exitCode !== 0 && !!stdError) {
                    reject(stdError);
                } else {
                    const result = {
                        output: stdOut,
                        warnings: stdError
                    };
                    resolve(result);
                };
            });
        });
    }
    

    /**
     * Copies a file from one location to another.
     * @param  {string} sourcePath
     * @param  {string} destinationPath
     */
    copyFile(sourcePath, destinationPath) {
        this.logAction(`> Copying \'${path.basename(sourcePath)}\' to \'${destinationPath}\'`);
        return FsExtra.copy(sourcePath, destinationPath)
        .catch(error => this.logError('developer-robot.js, copyFile(): ' + error.message));
    }


    getJsonFile(filePath) {
        return new Promise((resolve, reject) => {
            fs.readFile(filePath, (error, data) => {
                if (error) reject(error);
                else resolve(JSON.parse(data));
            });
        }).catch(error => this.logError('developer-robot.js, getJsonFile(): ' + error.message));
    }

}