'use strict';
const DeveloperRobot = require('./developer-robot');
const GulpUtil = require('gulp-util');

module.exports = class PythonBackEnd {
    
    constructor(
        backEndDirectoryPath,
        environmentDirectoryPath
    ) {
        this.developerRobot = new DeveloperRobot();
        this.backEndPath = backEndDirectoryPath;
        this.environmentPath = environmentDirectoryPath;

        this.getBackEndConfig = (environmentName) => this.developerRobot.getJsonFile(`${this.environmentPath}/options/${environmentName}/back-end-config.json`);
        this.projectType = 'Python Back End';
    }

    /**
     * Log an action to the console while completing tasks. 
     * @param  {string} logText
     * @return void
     */
    logAction(logText) {
        return console.log('\r\n\r\n' + GulpUtil.colors.bold.green.italic(logText) + '\r\n');
    }


    /**
     * Log an error message to the console while completing tasks. 
     * @param  {string} logText
     * @return void
     */
    logError(logText) {
        return console.log('\r\n\r\n' +GulpUtil.colors.bold.red(logText) + '\r\n\r\n');
    }


    /**
     * Gets the current environment name from the back end config file.
     */
    getCurrentEnvironmentName() {
        return this.developerRobot.getJsonFile(`${this.backEndPath}/back-end-config.json`)
            .then(backEndConfig => {
                this.logAction(`Python Back End Environment: ${backEndConfig.name.toUpperCase()}`);
                return backEndConfig.name;
            }, noFileError => 'No environment set. Run environment command!')
            .catch(error => this.logError(`python-back-end.js, getCurrentEnvironmentName(): ${error.message}`));
    }


    //console.log(gulpUtil.colors.blue('^ GCP project id set to ' + shell.exec('gcloud config get-value project')))

    /**
     * Changes the environment of the Python app. 
     * @param {string} environmentName 
     */
    changeEnvironment(environmentName) {
        const copyConfigFileToBackEndFolder = (fileName) => this.developerRobot.copyFile(`${this.environmentPath}/options/${environmentName}/${fileName}`, this.backEndPath + '/' + fileName);
        return copyConfigFileToBackEndFolder('app.yaml')
        .then(() => copyConfigFileToBackEndFolder('back-end-config.json'))
        .then(() => copyConfigFileToBackEndFolder('front-end-config.json'))
        .then(() => this.getBackEndConfig(environmentName))
        .then(backEndConfig => this.developerRobot.runCommand(`gcloud config set project ${backEndConfig.gcloudProject}`))
        .then(() => {
            this.logAction('(Below) Display the current GCloud Project ID: ')
            return this.developerRobot.runCommand('gcloud config get-value project', true);
        })
        .then(() => this.logAction(`PYTHON BACK END IS NOW IN ${environmentName.toUpperCase()} ENVIRONMENT`))
        .catch(error => this.logError(`python-back-end.js, changeEnvironment(): ${error.message}`));
    }
}