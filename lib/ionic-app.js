'use strict';
const Gulp = require('gulp');
const GulpUtil = require('gulp-util');
const Cheerio = require('gulp-cheerio');
const DeveloperRobot = require('./developer-robot');
const path = require('path');

const fs = require('fs');
//const Argv = require('yargs').Argv;

module.exports = class IonicApp {
    /**
     * Construct the Trillium Front End class.
     * @param {string} appDirectoryPath 
     * @param {string} environmentDirectoryPath 
     */
    constructor(
        appDirectoryPath,
        environmentDirectoryPath
    ) {
        this.developerRobot = new DeveloperRobot();
        this.appDirectoryPath = appDirectoryPath;
        this.environmentDirectoryPath = environmentDirectoryPath;
        //this.environmentRequirePath = `./../../../${environmentDirectoryPath}`;

        this.getFrontEndConfig = (environmentName) => this.developerRobot.getJsonFile(`${environmentDirectoryPath}/options/${environmentName}/front-end-config.json`);
        this.projectType = 'Ionic App';
    }


    /**
     * Log an action to the console while completing tasks. 
     * @param  {string} logText
     * @return void
     */
    logAction(logText) {
        return console.log('\r\n\r\n' + GulpUtil.colors.bold.green(logText) + '\r\n');
    }


    /**
     * Log an error message to the console while completing tasks. 
     * @param  {string} logText
     * @return void
     */
    logError(logText) {
        return console.log('\r\n\r\n' +GulpUtil.colors.bold.red(logText) + '\r\n\r\n');
    }


    /**
     * Gets the current environment name from the front end config file.
     */
    getCurrentEnvironmentName() {
        return this.developerRobot.getJsonFile(`${this.appDirectoryPath}/src/environment/front-end-config.json`)
            .then(frontEndConfig => {
                this.logAction(`Ionic App Environment: ${frontEndConfig.name.toUpperCase()}`);
                return frontEndConfig.name;
            }, noFileError => 'No environment set. Run environment command!')
            .catch(error => this.logError(`ionic-app.js, getCurrentEnvironmentName(): ${error.message}`));
    }

    
    /**
     * Switch between development environments for the front end app.
     *      - 
     * @param  {string} environmentName - Usually 'development' or 'production'
     * @return Promise<void>
     */
    changeEnvironment(environmentName) {
        try {
            const appendEnvConfigPath = (pathExtension) => this.environmentDirectoryPath + '/' + pathExtension;
            const appendAppPath = (pathExtension) => this.appDirectoryPath + '/' + pathExtension;

            const doesEnvironmentDirectoryExist = fs.existsSync(appendEnvConfigPath(`options/${environmentName}`));
            if (!doesEnvironmentDirectoryExist) throw new Error('Can\'t find the root environment directory! Please provide an accurate path to your environment variables.');

            // {next 6 lines} Get the new Front and Back end config file paths and destinations,
            // then copy them to the root environment directory.
            const frontEndFilePath = appendEnvConfigPath('options/' + environmentName + '/front-end-config.json');
            const frontEndFileDestination = appendAppPath('src/environment/front-end-config.json');

            const sourceIconPath = appendEnvConfigPath('ionic-images/' + environmentName + '-icon.png');
            const sourceSplashPath = appendEnvConfigPath('ionic-images/' + environmentName + '-splash.png');
            const ionicResourcesPath = appendAppPath('resources/');
            const doNewImageFilesExist = fs.existsSync(sourceIconPath) && fs.existsSync(sourceSplashPath);
            const doesIonicResourcesDirectoryExist = fs.existsSync(ionicResourcesPath);
            const doesIonicEnvironmentDirectoryExist = fs.existsSync(appendAppPath('src/environment/'));
            
            // All actions represented here as promises
            const copyFrontEndFile = this.developerRobot.copyFile(frontEndFilePath, frontEndFileDestination);
            const createResourcesDirectory = this.developerRobot.runCommand(`mkdir ${ionicResourcesPath}`);
            const createIonicEnvDirectory = this.developerRobot.runCommand(`mkdir ${appendAppPath('src/environment')}`);
            const copyIonicIconImg = this.developerRobot.copyFile(sourceIconPath, ionicResourcesPath + 'icon.png');
            const copyIonicSplashImg = this.developerRobot.copyFile(sourceSplashPath, ionicResourcesPath + 'splash.png');
            const copyIconToIonicAssets = this.developerRobot.copyFile(sourceIconPath, appendAppPath(`src/assets/imgs/icon.png`));
            const copySplashToIonicAssets = this.developerRobot.copyFile(sourceSplashPath, appendAppPath(`src/assets/imgs/splash.png`));

            // The array of all promises to be completed in this function.
            let toDoList = [
                copyFrontEndFile
            ];
            
            if (!doesIonicEnvironmentDirectoryExist) {
                toDoList = [
                    createIonicEnvDirectory,
                    copyFrontEndFile
                ];
            }

            if (doNewImageFilesExist) {
                if (!doesIonicResourcesDirectoryExist) toDoList.push(createResourcesDirectory);
                toDoList.push.apply(toDoList, [
                    copyIonicIconImg,
                    copyIonicSplashImg,
                    copyIconToIonicAssets,
                    copySplashToIonicAssets
                ]);
            }

            const completeToDoList = () => new Promise((resolve, reject) => {
                let index = 0;
                const recursivelyCompleteActions = function completeNextAction() {
                    let actionError = undefined;
                    toDoList[index].then(() => {
                        index++;
                        if (!!actionError) {
                            reject(actionError);
                        } else if (index === toDoList.length) {
                            resolve();
                        } else {
                            completeNextAction();
                        }
                    }, error => {
                        actionError = error;
                        reject(error);
                    });
                };
                recursivelyCompleteActions();
            });

            return this.getFrontEndConfig(environmentName)
            .then(frontEndFile => {
                const newMobileAppId = frontEndFile.ionicApp.appId;
                const newMobileAppName = frontEndFile.ionicApp.appName;
                const newMobileAppVersion = frontEndFile.ionicApp.version;
                return this.changeIonicConfigXML(newMobileAppName, newMobileAppId, newMobileAppVersion);
            })
            .then(() => completeToDoList())
            .then(() => this.reinstallPlatforms())
            .then(() => this.logAction(`THE IONIC APP IS NOW IN THE ${environmentName.toUpperCase()} ENVIRONMENT`))
            .catch(error => {
                this.logError('ionic-app.js, changeEnvironment(): Promise Catch - ' + error.message);
            });

        } catch(e) {
            this.logError('ionic-app.js, changeEnvironment(): Catch block - ' + e.message);
        }
    }


    /**
     * Makes changes to the ionic_app/config.xml file using the Gulp-Cheerio plugin.
     * @param  {string} newAppName
     * @param  {string} newAppId
     * @param  {string} newAppVersion
     * @returns Promise<void>
     */
    changeIonicConfigXML(newAppName, newAppId, newAppVersion) {
        this.logAction(` Changing the ${this.appDirectoryPath}/config.xml variables:\r\n` +
                    `      App Name: ${newAppName}\r\n` +
                    `      App ID: ${newAppId}\r\n` +
                    `      App Version: ${newAppVersion}\r\n\r\n`                    
        );

        let doesFileExist = false;
        return new Promise((resolve, reject) => {
            Gulp.src(`${this.appDirectoryPath}/config.xml`)
            .pipe(Cheerio({
                parserOptions: {
                    xmlMode: true
                },
                run: function ($, file, done) {
                    doesFileExist = !!file;
                    $('widget name').text(newAppName);
                    $('widget').each(function () {
                        $(this).attr('id', newAppId);
                        $(this).attr('version', newAppVersion);
                    });
                    done();
                }
            }))
            .pipe(Gulp.dest(this.appDirectoryPath))
            .on('finish', () => {
                if (!doesFileExist) {
                    const invalidPathError = new Error('ionic-app.js, changeIonicConfigXML(): The referenced path to the config.xml file doesn\'t exist.');
                    reject(invalidPathError);
                } else {
                    resolve();
                }
            });
        });
    }


    /**
     * Installs NPM packages in the Ionic app.
     * @returns {Promise} Promise<commandResult>
     * @returns Promise<void>
     */
    installPackages() {
        this.logAction('Reinstalling NPM packages in the Ionic App...')
        return this.developerRobot.runCommand(`cd ${this.appDirectoryPath} && npm i`, true)
        .catch(error => console.error('ionic-app.js, installPackages(): ' + error.message));
    }


    /**
     * Installs an NPM package in the ionic app.
     * @param  {string} packageNameOrUrl
     * @returns Promise<void>
     */
    installPackage(packageNameOrUrl) {
        this.logAction(`Installing ${packageNameOrUrl} NPM package in the Ionic App...`);
        return this.developerRobot.runCommand(`cd ${this.appDirectoryPath} && npm i ${packageNameOrUrl}`, true)
        .catch(error => console.error('ionic-app.js, installPackage(): ' + error.message));
    }


    /**
     * Installs an NPM package in the ionic app.
     * @param  {string} packageNameOrUrl
     * @returns Promise<void>
     */
    uninstallPackage(packageNameOrUrl) {
        this.logAction(`Uninstalling ${packageNameOrUrl} NPM package from the Ionic App...`)
        return this.developerRobot.runCommand(`cd ${this.appDirectoryPath} && npm uninstall ${packageNameOrUrl}`, true)
        .catch(error => console.error('ionic-app.js, uninstallPackage(): ' + error.message));
    }


    /**
     * Installs a Cordova plugin in the ionic app.
     * @param  {string} pluginNameOrUrl
     */
    installPlugin(pluginNameOrUrl) {
        this.logAction(`Installing the ${pluginNameOrUrl} Cordova plugin in the Ionic App...`)
        return this.developerRobot.runCommand(`cd ${this.appDirectoryPath} && ionic cordova plugin add ${pluginNameOrUrl}`, true)
        .catch(error => console.error('ionic-app.js, installPlugin(): ' + error.message));
    }


    /**
     * Uninstalls a Cordova plugin in the ionic app.
     * @param  {string} pluginNameOrUrl
     */
    uninstallPlugin(pluginNameOrUrl) {
        this.logAction(`Uninstalling the ${pluginNameOrUrl} Cordova plugin from the Ionic App...`)
        return this.developerRobot.runCommand(`cd ${this.appDirectoryPath} && ionic cordova plugin rm ${pluginNameOrUrl}`, true)
        .catch(error => console.error('ionic-app.js, uninstallPlugin(): ' + error.message));
    }


    /**
     * Reinstalls a Cordova plugin in the ionic app.
     * @param  {string} pluginNameOrUrl
     */
    reinstallPlugin(pluginNameOrUrl) {
        this.logAction(`Reinstalling the ${pluginNameOrUrl} Cordova plugin in the Ionic App...`)
        return this.developerRobot.runCommand(`cd ${this.appDirectoryPath} && ionic cordova plugin rm ${pluginNameOrUrl}; ionic cordova plugin add ${pluginNameOrUrl}`, true)
        .catch(error => console.error('ionic-app.js, uninstallPlugin(): ' + error.message));
    }

    /**
     * Installs a platform; either 'ios' or 'android'
     * @param {string} platformName 
     */
    installPlatform(platformName) {
        this.logAction(`Installing the ${platformName} platform...`)
        return this.developerRobot.runCommand(`cd ${this.appDirectoryPath} && ionic cordova platform add ${platformName}`, true)
        .catch(error => console.error('ionic-app.js, installPlatform(): ' + error.message));
    }

    /**
     * Uninstalls a platform; either 'ios' or 'android'
     * @param {string} platformName 
     */
    uninstallPlatform(platformName) {
        this.logAction(`Uninstalling the ${platformName} platform...`)
        return this.developerRobot.runCommand(`cd ${this.appDirectoryPath} && ionic cordova platform rm ${platformName}`, true)
        .catch(error => console.error('ionic-app.js, uninstallPlatform(): ' + error.message));
    }


    /**
     * Reinstall a platform; either 'android' or 'ios'
     * @param  {string} platformName
     */
    reinstallPlatform(platformName) {
        this.logAction(`Reinstalling the ${platformName} platform...`)
        return this.developerRobot.runCommand(`cd ${this.appDirectoryPath} && ionic cordova platform rm ${platformName}; ionic cordova platform add ${platformName}`, true)
        .catch(error => console.error('ionic-app.js, reinstallPlatform(): ' + error.message));
    }


    /**
     * Reinstall both android and ios platforms
     * @returns Promise<void>
     */
    reinstallPlatforms() {
        this.logAction(`Reinstalling the android and ios platforms...`)
        return this.developerRobot.runCommand(
            `cd ${this.appDirectoryPath} && ` +
            'ionic cordova platform rm ios; ' +
            'ionic cordova platform rm android; ' + 
            'ionic cordova platform add ios;' + 
            'ionic cordova platform add android; ' 
        , true)
        .catch(error => console.error('ionic-app.js, reinstallPlatform(): ' + error.message));
    }


    /**
     * Emulates on the developer's machine based on the given parameters.
     * @param  {object} args
     * @returns Promise<void> Emulates to the device
     */
    emulate(args) {
        const isEmulatingIos = !!args.ios || !!args.i;
        const isEmulatingAndroid = !!args.android || args.a; 
        const deviceFromUser = args.device ? args.device: args.d;
        const emulatorPlatform = isEmulatingIos ? 'ios' : isEmulatingAndroid ? 'android' : undefined;
        const isLiveReload = !!args.l || !!args.livereload;

        let emulatorDevice = '';
        let command = `cd ${this.appDirectoryPath} && `;
        if (isEmulatingIos) {
            if(!!deviceFromUser) {
                emulatorDevice = deviceFromUser;
            } else {
                emulatorDevice = 'iPhone-7';
            }
            command += 'ionic cordova emulate ios --target ' + emulatorDevice;
        } else if (isEmulatingAndroid) {
            if(!!deviceFromUser) {
                emulatorDevice = deviceFromUser;
            } else {
                emulatorDevice = '';
            }
            command += 'ionic cordova emulate android --target ' + emulatorDevice;
        }

        if (isLiveReload) command += ' -l';
        console.log('COMMAND: ', command);
        
        this.logAction(`Emulating ${emulatorPlatform} ${!!emulatorDevice ? ' on ' + emulatorDevice : ''}`)
        return this.developerRobot.runCommand(command, true)
        .catch(error => console.error(`ionic-app.js, emulate(): ${error.message}`))
    }

    /**
     * Run the app on a connected device.
     * @param  {object} args - The args provided from a Gulp command with yargs.argv
     *  Examples: 
     *      gulp run -ios
     *      gulp run -android
     *      gulp run -i
     *      gulp run -i -a
     */
    run(args) {
        const isRunningAndroid = !!args.android || !!args.a; 
        const isRunningIos = !!args.ios || !!args.i;
        const isRunningBothPlatforms = !!args.b || !!args.both;
        const platformToRun = isRunningIos ? 'ios' : 'android';
        let command = `cd ${this.appDirectoryPath} && `;
        command += !isRunningBothPlatforms ? 
            'ionic cordova run ' + platformToRun + ' --device' :
            'ionic cordova run android --device; ionic cordova run ios --device'
        return this.developerRobot.runCommand(command)
        .catch(error => this.logError(`ionic-app.js, run(): ${error.message}`));
    }
    
}