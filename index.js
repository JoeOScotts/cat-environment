const DeveloperRobot = require('./lib/developer-robot');
const IonicApp = require('./lib/ionic-app');
const PythonBackEnd = require('./lib/python-back-end');
const path = require('path');
const gulpUtil = require('gulp-util');

/**
 * This is the class for the Trillium Environment Tools. Import it with a require 
 * statement, then instantiate it with 'new TrillEnvironmentTools(frontEndPath, backEndPath, envPath);
 */
class ProjectEnvironmentTools {

    constructor(
    ) {
        this.activeEnvironments = [];
    }


    /**
     * Initializes the environment package base on the provided paramters.
     * @param {object} configObject - Object which specifies directory locations. Ex:
     * {
     *      ionicApp: "./ionic-app",
     *      pythonBackEnd: "./back-end",
     *      environment: "./environment"
     * } 
     */
    initialize(configObject) {
        try {
            this.envPath = configObject['environment'];
            this.configObject = configObject;
            if (!configObject || typeof configObject !== 'object' || Object.keys(configObject).length < 2) throw new Error('Invalid config object. Please provide valid configuration parameters');
            if (!configObject['environment']) throw new Error('Invalid config object. Please provide the \'environment\' property as a string which specifies the path to your environment folder.'); 
            const appNames = Object.keys(configObject).filter(name => name !== 'environment');
            appNames.forEach(name => this[`${name}Path`] = path.normalize(configObject[name]));
            this.environmentInitialized = true;
        } catch(error) {
            this.logError(`cat-environment, index.js, initializeEnvironment(): ${e.message}`)
        }
    }


    /**
     * Log an action to the console while completing tasks. 
     * @param  {string} logText
     * @return void
     */
    logAction(logText) {
        return console.log('\r\n\r\n' + gulpUtil.colors.bold.cyan(logText) + '\r\n');
    }


    /**
     * Log an error message to the console while completing tasks. 
     * @param  {string} logText
     * @return void
     */
    logError(logText) {
        return console.log('\r\n\r\n' + gulpUtil.colors.bold.red(logText) + '\r\n\r\n');
    }


    /**
     * Instantiate a new developer robot class.
     */
    developerRobot() {
        return new DeveloperRobot();
    }


    /**
     * Instantiate a new IonicApp environment class
     */
    ionicApp() {
        try {
            if (!this.environmentInitialized) throw new Error('No environment credentials loaded. You must call initializeEnvironment() before instantiating the Ionic App!')
            if (!this.configObject['ionicApp']) throw new Error('The Ionic App directory path isn\'t defined. Have you provided it in the initializeEnvironment() parameters?');
            const ionicEnvironmentClass = new IonicApp(this.ionicAppPath, this.envPath);
            this.activeEnvironments.push(ionicEnvironmentClass);
            return ionicEnvironmentClass;
        } catch(e) {
            this.logError(`cat-environment, index.js, ionicApp(): ${e.message}`)
        }
    }


    /**
     * Instantiate a new PythonBackEnd environment class
     */
    pythonBackEnd() {
        try {
            if (!this.environmentInitialized) throw new Error('No environment credentials loaded. You must call initializeEnvironment() before instantiating the Python Back End!')
            if (!this.configObject['pythonBackEnd']) throw new Error('The Python Back End App directory path isn\'t defined. Have you provided it in the initializeEnvironment() parameters?');
            const pythonBackEndEnvironmentClass = new PythonBackEnd(this.pythonBackEndPath, this.envPath);
            this.activeEnvironments.push(pythonBackEndEnvironmentClass);
            return pythonBackEndEnvironmentClass;
        } catch(e) {
            this.logError(`cat-environment, index.js, pythonBackEnd(): ${e.message}`)
        }
    }


    /**
     * Changes all active environments to the provided environment name.
     * @param {string} environmentName 
     */
    changeEnvironments(environmentName) {
        try {
            if (!this.environmentInitialized) throw new Error('No environment credentials loaded. You must call initializeEnvironment() before changing environments!')
            const self = this;
            return new Promise((resolve, reject) => {
                let index = 0;
                let envError = undefined;
                const recursivelyChangeEnvs = function changeNextEnvironment() {
                    self.activeEnvironments[index].changeEnvironment(environmentName).then(() => {
                        index++;
                        if (!!envError) {
                            reject(envError);
                        } else if (index === self.activeEnvironments.length) {
                            resolve();
                        } else {
                            changeNextEnvironment();
                        }
                    }, error => {
                        envError = error;
                        reject(error);
                    });
                };
                recursivelyChangeEnvs();
            })
            .then(() => this.logAction(`SUCCESS! YOU\'RE NOW IN THE ${environmentName.toUpperCase()} ENVIRONMENT!\r\n`))
            .catch(error => this.logError(`cat-environment, index.js, changeEnvironments(): ${error.message}`));
        } catch (e) {
            this.logError(`cat-environment, index.js, changeEnvironments(): ${e.message}`)
        }
    }


    /**
     * Prints the current environment names to the console
     */
    getCurrentEnvironmentNames() {
        const self = this;
        let allEnvNamesObj = {};
        return new Promise((resolve, reject) => {
            let index = 0;
            let envError = undefined;
            const getEnvNames = function getNextEnvName() {
                const projectType = self.activeEnvironments[index].projectType;
                self.activeEnvironments[index].getCurrentEnvironmentName()
                .then(envName => {
                    allEnvNamesObj[projectType] = envName;
                    index++;
                    if (!!envError) {
                        reject(envError);
                    } else if (index === self.activeEnvironments.length) {
                        resolve();
                    } else {
                        getNextEnvName();
                    }
                }, error => {
                    envError = error;
                    reject(error);
                })
            }
            getEnvNames();
        }).then(() => {
            const red = (text) => gulpUtil.colors.bold.red(text);
            const blue = (text) => gulpUtil.colors.bold.cyan(text);
            const yellow = (text) => gulpUtil.colors.bold.yellow(text);

            console.log(blue('\r\nCurrent Environments: \n'));
            Object.keys(allEnvNamesObj).forEach(envKey => {
                const envName = allEnvNamesObj[envKey].toUpperCase();
                const envMessage = blue(`>    ${envKey}: `) + ( envName === 'PRODUCTION' || envName === 'PROD' ? red(envName) : yellow(envName) );
                console.log(envMessage);
            });
            console.log('\n');
            return allEnvNamesObj;
        });
    }
}

module.exports = new ProjectEnvironmentTools();