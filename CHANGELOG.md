# [1.0.0] - 2018-06-15
## Github Tag: [1.0.0]

## Added:
- Ionic App getCurrentEnvironmentName() - ability to get the current environment name.
- Python Back End getCurrentEnvironmentName() - ability to get the current environment name.
- CAT Environment ability to display names of all active environments. index.js, getCurrentEnvironments().
- Added the 'projectType' properties to Ionic App and Python Back end. This is just a string which describes the project type.

## Changed 
- Modified the Python Back End the change the gcloud project when changing environments. All `back-end-config.json`'s with a Python Back End must have a  `gcloudProject` property now.
- Changed the Ionic App changeEnvironment() fn to copy the `icon.png` and `splash.png` images into the `ionic-app/src/assets` directory.

## Fixes
- NONE. MY CODE IS PERFECT. - Joe O